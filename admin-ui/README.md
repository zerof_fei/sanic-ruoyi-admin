<h1 align="center" style="margin: 30px 0 30px; font-weight: bold;">RuoYi v3.8.7</h1>
<h4 align="center">基于sanic + 若依（Vue3版本）前后端分离的快速开发框架</h4>


### 平台简介
* 前端技术栈 [Vue3](https://v3.cn.vuejs.org) + [Element Plus](https://element-plus.org/zh-CN) + [Vite](https://cn.vitejs.dev) 若依框架版本。
  

### 前端运行

```
# 安装依赖
yarn --registry=https://registry.npmmirror.com

# 启动服务
yarn dev

# 构建测试环境 yarn build:stage
# 构建生产环境 yarn build:prod
# 前端访问地址 http://localhost:80
```

### 官方体验地址
- 演示地址：http://vue.ruoyi.vip  
- 账号密码：admin/admin123