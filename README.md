<h1 align="center" style="margin: 30px 0 30px; font-weight: bold;">sanic-ruoyi-admin</h1>
<h4 align="center">基于sanic + 若依（Vue3版本）快速开发框架（v1.0.1）</h4>

<p align="center">
	👉 <a target="_blank" href="http://124.71.212.219:8028/">个人网站：http://124.71.212.219:8028/</a> 👈
</p>

<p align="center">
	<a href="https://gitee.com/mengyinggitee/sanic-vue-admin/stargazers"><img src="https://gitee.com/mengyinggitee/sanic-vue-admin/badge/star.svg"></a>
	<a href="https://gitee.com/mengyinggitee/sanic-vue-admin/members"><img src="https://gitee.com/mengyinggitee/sanic-vue-admin/badge/fork.svg"></a>
	<a><img src="https://img.shields.io/badge/QQ-652044581-green"></a>
	<a><img src=https://img.shields.io/badge/%E5%BE%AE%E4%BF%A1-17783098377-brightgreen"></a>	
	<a><img src=https://img.shields.io/badge/python-3.70-black"></a>
</p>

### 开发背景
* 若依是一套全部开源的快速开发平台，[若依框架地址]([Vue3](https://v3.cn.vuejs.org))， 但目前主流的若以框架是基于SpringBoot的java开发语言，官网给出的python版本源码从技术栈来看，存在一下问题：

1. 后端采用django框架，框架大而重，和本项目快速开发的主题思想不符且sql语法封装不够，项目启动需要配置过多
2. python由于全局解释器锁的原因，django采用同步写法在并发上和采用异步语法的sanic框架相比，性能上存在一定差距， 
3. 若依框架python版本的前端框架页面太过浮夸， [演示地址](http://demo.django-vue-admin.com )，不太适合大部分的业务场景，且广告页面太多

* 基于以上情况，萌生了调试一套适合python程序员的快速开发框架，删除广告页面和日常开发者不必要的功能，只保留项目的最核心的配置模块，减少初始化的方式，做到入手既可以开发功能

### 优化方向
* 后端：采用sanic框架, 采用纯异步语法，保障后端服务性能和开发效率
* 后端：采用mongodb数据库，封装mongo语法，能够快速开发和拓展，追求极致的快速开发
* 后端：收集日常的常用方法，增加常用的企业微信机器人通知，数据加解密等常用方法
* 后端：数据库配置增加nacos和本地config的配置的2种配置，多种配置方式
* 后端：配置数据库后，项目启动直接初始化相关数据，不需要导入sql的方式，方便快捷开发进度
* 前端：删除不必要的广告页面，和不必要的功能，比如日志，人员职位，登陆等相关信息，只保留用户，角色，目录，部门核心的模块功能
* 前端：收集封装日常中大佬们封装的一些方法和看到的一些好的方法，实现工具箱似的开发，开箱即用
* 前端：优化若依框架的部分ui和提取字段，如title，base_path等变为env配置字段和调试时候的vite代理等均有展示


### 技术栈介绍
* 前端技术栈： [Vue3](https://v3.cn.vuejs.org) + [Element Plus](https://element-plus.org/zh-CN) + [Vite](https://cn.vitejs.dev) 若依框架版本。
* 后端技术栈： [sanic](https://sanic.dev/zh/) + [mongo](https://mongodb.p2hp.com/) + [redis](https://redis.io/) 异步io实现。

<table>
    <tr>
        <td style="width:400px"><img src="./admin-ui/src/assets/images/20240418233140.png"/></td>
    </tr>
<table>


### 前端部署
```
# 本地部署
# 安装依赖
yarn --registry=https://registry.npmmirror.com

# 启动服务
yarn run dev

# 打包构建
yarn build:prod

# 前端访问地址 
http://localhost:80

# api地址修改：
vite.config.js文件 server.proxy.target指向后端地址

=======================================================================
# 线上docker部署
# 前提确认宿主机含有node环境，(调试环境) node_version = v21.1.0

# api地址修改：
.env.production文件  VITE_APP_BASE_API 属性

# 构建命令
sh build.sh
```

### 后端部署
```
本地部署
# 进入目录
cd admin-api

# 安装依赖
pip install -r requirements.txt 

# 配置数据库
/config.py 或者 配置nacos地址 （配置mongo和redis的数据）

# 启动服务
python main.py

=======================================================================
# docker部署（docker-compose）
docker-compose build && docker-compose up -d

# 或者执行脚本构建
sh build.sh

内置一个超级管理员账号
superAdmin/superAdmin
```
### 项目体验地址

演示地址：[http://124.71.212.219:8101/#/login](http://124.71.212.219:8101/#/login)

账号密码：superAdmin/superAdmin

如果能帮助到您，快速的构建您的项目，麻烦帮我star一下吧，谢谢咯，如果有问题的请加我微信


## 演示图

<table>
    <tr>
        <td style="width:400px"><img src="./admin-ui/src/assets/images/gitshow1.jpg"/></td>
        <td style="width:400px"><img src="./admin-ui/src/assets/images/gitshow2.jpg"/></td>
    </tr>
    <tr>
        <td style="width:400px"><img src="./admin-ui/src/assets/images/gitshow3.jpg"/></td>
        <td style="width:400px"><img src="./admin-ui/src/assets/images/gitshow4.jpg"/></td>
    </tr>
    <tr>
        <td style="width:400px"><img src="./admin-ui/src/assets/images/gitshow5.jpg"/></td>
        <td style="width:400px"><img src="./admin-ui/src/assets/images/gitshow6.jpg"/></td>
    </tr>
    
<table>