
---
### 备注，当前python版本,建议 Python3.9 - 3.9.19

---

### 记录，运行,遇到的一些问题

1. `ImportError: cannot import name 'coroutine' from 'asyncio'` 需要本地安装一下 

   # [Having an import error for cannot import name coroutine from asyncio python](https://stackoverflow.com/questions/77730349/having-an-import-error-for-cannot-import-name-coroutine-from-asyncio-python)

   > pip install asyncio
   >
   > pip show motor
   >
   > pip install motor==3.4.0

2. 多进程共享缓存

   > 参考文案地址: https://blog.csdn.net/qq_41591215/article/details/133746960
   >
   > 核心: multiprocessing
   >
   > 代码
   >
   > ```python
   > import multiprocessing
   > from sanic import HTTPResponse, Sanic, response
   > from sanic.log import logger
   > 
   > app = Sanic("Hw-Licence-System")
   > app.config.REQUEST_TIMEOUT = 180
   > 
   > 
   > # 创建共享的Manager对象
   > @app.main_process_start
   > async def main_process_start(app):
   >     app.shared_ctx.cache = multiprocessing.Manager().dict()
   > 
   > @app.route("/api/v1/get_data", methods=["GET"])
   > async def get_data(request):
   >     product_name = request.args.get("product_name")
   >     shared_cache = request.app.shared_ctx.cache
   >     # 尝试从共享缓存中获取数据
   >     if product_name in shared_cache:
   >         data = shared_cache[product_name]
   >         return response.json({"status": True, "data": data})
   > 
   >     # 存储到缓存
   >     logger.info("get data from server")
   >     shared_cache[product_name] = "123"
   >     # 获取数据并返回
   >     if product_name in shared_cache:
   >         data = shared_cache[product_name]
   >         return response.json({"status": True, "data": data})
   >     else:
   >         return response.json({"status": False, "message": "Data not found"})
   >     
   >     
   > if __name__ == "__main__":
   >     app.run(host="0.0.0.0", port=3000, workers=4)
   > 
   > ```
