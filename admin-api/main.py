# -*- coding: utf-8 -*- 
"""
========================================================================================================================
@project : my-sanic
@file: main
@Author: mengying
@email: 652044581@qq.com
@date: 2023/3/3 10:04
@desc: 项目主入口启动文件
========================================================================================================================
"""
import os
from sanic import Sanic
from config import config
from sanic_cors import CORS


# 初始化项目实例
app = Sanic(config.PROJECT_NAME)

# 服务配置跨域需求（默认可以跨域）
CORS(app)

# 配置引入服务监听器
from middleware import myListener

# 配置引入中间件
from middleware import myMiddleware
from middleware import mobileAuthorization
    
# 配置注册项目蓝图
from api import urls
app.blueprint(urls.routers)


if __name__ == '__main__':    
    import platform
    if platform.system().lower() == "windows":
        app.run(host='127.0.0.1', port=8085, auto_reload=True, access_log=True, workers=1)
    else:
        app.run(host='0.0.0.0', port=8085, auto_reload=True, access_log=True, workers=os.cpu_count())
        # app.run(host='0.0.0.0', port=8085, workers=1)
