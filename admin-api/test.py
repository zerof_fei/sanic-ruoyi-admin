# 测试redis
import asyncio
from addict import Dict as asDict
from utils.myCache import Cache,RedisSyncTool, RedisAsyncTool
from config import config
from redis import Redis, RedisCluster
from aredis import StrictRedis, StrictRedisCluster
def testRedisConnectIsOk():
    # 配置redis缓存组件, 前两项固定配置
    c = Cache()

    asyncio.run(c.testRedisConnectByAsync())

# testRedisConnectIsOk()

def testRedisBySync():
    # 默认写法
    # redisClient = Cache().select('redis').current_db
    # redisClient.set('3128ae36eae9491cbd04b478acdcb1b1', 'aaaaa')
    # redisClient.expire('3128ae36eae9491cbd04b478acdcb1b1',10)
    # 新增工具快捷调用
    RedisAsyncTool().set('3128ae36eae9491cbd04b478acdcb1b1','aaaaa',10)
# testRedisBySync()

async def testRedisByAsync():
    uid = '0a58fb6d9792493f97fa241d153a3872'
    await RedisAsyncTool().set(uid,'3861')
    # db = Cache(mode="async").select('redis').current_db
    authCode = await RedisAsyncTool().get(uid)
    # await RedisAsyncTool().delete('0a58fb6d9792493f97fa241d153a3872')
    if not authCode:
        print('空 - authCode')
    else:
        print('getCode: {}'.format(authCode))


asyncio.run(testRedisByAsync())

# def getAuthCode():
#     redisC = Cache().select('redis').current_db
#     authCode = redisC.get('3128ae36eae9491cbd04b478acdcb1b1')
#     print('authCode: {}'.format(authCode))

# getAuthCode()
# 测试异步
# async def main():
#     print('开始执行')
#     await asyncio.sleep(1)
#     print('异步执行')
    
# asyncio.run(testRedisConnectIsOk())

# 测试 redis
# async def example():
#     client = StrictRedis(host='127.0.0.1', port=6379, db=0,password="mall-redis-6379")
#     print('client： ',client)
#     await client.flushdb()
#     await client.set('foo', 1)
#     assert await client.exists('foo') is True
#     await client.incr('foo', 100)

#     assert int(await client.get('foo')) == 101
#     await client.expire('foo', 1)
#     await asyncio.sleep(0.1)
#     await client.ttl('foo')
#     await asyncio.sleep(1)
#     assert not await client.exists('foo')

# asyncio.run(example())