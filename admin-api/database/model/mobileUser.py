# -*- coding: utf-8 -*-
"""
@Author：ironman
@file： mobUser.py
@date：2024/05/27 21:11
@email: zerof_fei@163.com
@desc: 系统用户数据库模型
"""
from marshmallow import Schema, fields
from database.mongoClient import AsyncMongoMethod

class MobileUser(Schema):
    """
    @description: 系统用户角色关系映射表
    """
    username = fields.String(dump_default="", metadata={"desc": "用户账号"})    
    password = fields.String(dump_default="", metadata={"desc": "密码（加密后）"})
    avatar = fields.String(dump_default="", metadata={"desc": "头像"})
    nickName = fields.String(dump_default="", metadata={"desc": "用户昵称"})
    phone = fields.String(dump_default="", metadata={"desc": "手机号码"})
    status = fields.String(dump_default="0", metadata={"desc": "帐号状态（0正常 1停用）"})
    delFlag = fields.String(dump_default="0", metadata={"desc": "删除标志（0代表存在 2代表删除）"})
    remark = fields.String(dump_default="", metadata={"desc": "备注"})

    class Meta:
        collection_name = "MobileUser"


class MobileUserDao(AsyncMongoMethod):
    """
    使用方法： 异步AsyncMongoMethod， 需指定_model_class的模型方法
    """

    _model_class = MobileUser