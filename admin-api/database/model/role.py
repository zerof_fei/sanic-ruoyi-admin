# -*- coding: utf-8 -*-
"""
@Author：mengying
@file： role.py
@date：2023/12/18 15:34
@email: 652044581@qq.com
@desc: 系统角色数据库模型
"""
from marshmallow import Schema, fields
from database.mongoClient import AsyncMongoMethod


class SystemRole(Schema):
    """
    @description: 系统角色表
    """
    roleId = fields.String(dump_default="", metadata={"desc": "角色id"})
    roleName = fields.String(dump_default="", metadata={"desc": "角色名称"})
    roleKey = fields.String(dump_default="", metadata={"desc": "角色权限字符"})
    roleSort = fields.Integer(dump_default=0, metadata={"desc": "角色排序"})
    roleAdmin = fields.Bool(dump_default=False, metadata={"desc": "是否是超级管理员"})
    status = fields.String(dump_default="0", metadata={"desc": "启用状态 （0正常 1停用）"})
    menuCheckStrictly = fields.Bool(dump_default=True, metadata={"desc": "菜单树选择项是否关联显示"})
    deptCheckStrictly = fields.Bool(dump_default=True, metadata={"desc": "部门树选择项是否关联显示"})
    delFlag = fields.String(dump_default="0", metadata={"desc": "是否删除 （0代表存在 2代表删除）"})
    menuIds = fields.List(fields.String(), metadata={"desc": "菜单权限"})
    remark = fields.String(dump_default="", metadata={"desc": "备注信息"})
    createBy = fields.String(dump_default="", metadata={"desc": "创建者"})

    class Meta:
        collection_name = "SystemRole"


class SystemRoleDao(AsyncMongoMethod):
    """
    使用方法： 异步AsyncMongoMethod， 需指定_model_class的模型方法
    """

    _model_class = SystemRole


class SystemRoleMenu(Schema):
    """
    @description: 系统用户角色关系映射表
    """
    menuId = fields.String(dump_default="", metadata={"desc": "菜单id"})
    roleId = fields.String(dump_default="", metadata={"desc": "角色id"})

    class Meta:
        collection_name = "SystemRoleMenu"


class SystemRoleMenuDao(AsyncMongoMethod):
    """
    使用方法： 异步AsyncMongoMethod， 需指定_model_class的模型方法
    """

    _model_class = SystemRoleMenu


if __name__ == '__main__':
    print(SystemRole().dump({'roleAdmin': True}))
