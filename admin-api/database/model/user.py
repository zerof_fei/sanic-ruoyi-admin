# -*- coding: utf-8 -*-
"""
@Author：mengying
@file： user.py
@date：2023/12/19 10:11
@email: 652044581@qq.com
@desc: 系统用户数据库模型
"""
from marshmallow import Schema, fields
from database.mongoClient import AsyncMongoMethod


class SystemUser(Schema):
    """
    @description: 系统用户表
    """
    userId = fields.String(dump_default="", metadata={"desc": "用户id"})
    deptId = fields.String(dump_default="", metadata={"desc": "部门id"})
    username = fields.String(dump_default="", metadata={"desc": "用户账号"})
    nickName = fields.String(dump_default="", metadata={"desc": "用户昵称"})
    userType = fields.String(dump_default="00", metadata={"desc": "用户类型（00系统用户）"})
    email = fields.String(dump_default="", metadata={"desc": "邮箱"})
    phone = fields.String(dump_default="", metadata={"desc": "手机号码"})
    avatar = fields.String(dump_default="", metadata={"desc": "头像"})
    password = fields.String(dump_default="", metadata={"desc": "密码（加密后）"})
    status = fields.String(dump_default="0", metadata={"desc": "帐号状态（0正常 1停用）"})
    delFlag = fields.String(dump_default="0", metadata={"desc": "删除标志（0代表存在 2代表删除）"})
    loginIp = fields.String(dump_default="", metadata={"desc": "最后登陆ip"})
    loginDate = fields.String(dump_default="", metadata={"desc": "最后登陆日期"})
    remark = fields.String(dump_default="", metadata={"desc": "备注"})
    createBy = fields.String(dump_default="", metadata={"desc": "创建者"})

    class Meta:
        collection_name = "SystemUser"


class SystemUserDao(AsyncMongoMethod):
    """
    使用方法： 异步AsyncMongoMethod， 需指定_model_class的模型方法
    """

    _model_class = SystemUser


class SystemUserRole(Schema):
    """
    @description: 系统用户角色关系映射表
    """
    userId = fields.String(dump_default="", metadata={"desc": "用户id"})
    roleId = fields.String(dump_default="", metadata={"desc": "角色id"})

    class Meta:
        collection_name = "SystemUserRole"


class SystemUserRoleDao(AsyncMongoMethod):
    """
    使用方法： 异步AsyncMongoMethod， 需指定_model_class的模型方法
    """

    _model_class = SystemUserRole
