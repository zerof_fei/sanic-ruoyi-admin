# -*- coding: utf-8 -*- 
"""
========================================================================================================================
@project : my-sanic
@file: model
@Author: mengying
@email: 652044581@qq.com
@date: 2023/3/15 16:59
@desc: 系统部门数据库模型
========================================================================================================================
"""
from marshmallow import Schema, fields
from database.mongoClient import AsyncMongoMethod
from enum import Enum, unique


@unique
class SystemStatusEnum(Enum):
    p0 = "0"  # 0正常
    p1 = "1"  # 1停用


@unique
class SystemDelEnum(Enum):
    p0 = "0"  # 0存在
    p1 = "2"  # 1删除


@unique
class SystemUserTypeEnum(Enum):
    p0 = "00"  # 系统账号
    p1 = "01"  # 管理员账号


class SystemDept(Schema):
    """
    @description: 系统部门表
    """
    parentId = fields.String(dump_default="", metadata={"desc": "父id"})
    deptName = fields.String(dump_default="", metadata={"desc": "部门名称"})
    deptId = fields.String(dump_default="", metadata={"desc": "部门id"})
    orderNum = fields.Integer(dump_default=0, metadata={"desc": "订单号"})
    leader = fields.String(dump_default="", metadata={"desc": "负责人"})
    phone = fields.String(dump_default="", metadata={"desc": "联系电话"})
    email = fields.String(dump_default="", metadata={"desc": "邮箱"})
    status = fields.String(dump_default="0", metadata={"desc": "启用状态 （0正常 1停用）"})
    delFlag = fields.String(dump_default="0", metadata={"desc": "是否删除 （0代表存在 2代表删除）"})
    ancestors = fields.String(dump_default="", metadata={"desc": "上级目录id"})
    createBy = fields.String(dump_default="", metadata={"desc": "创建者"})

    class Meta:
        collection_name = "SystemDept"


class SystemDeptDao(AsyncMongoMethod):
    """
    使用方法： 异步AsyncMongoMethod， 需指定_model_class的模型方法
    """

    _model_class = SystemDept


class SystemInit(Schema):
    """
    @description: 系统初始化标识
    """
    Init = fields.Bool(metadata={"desc": "启用状态 （True已初始化）"})
    # Name = fields.String(dump_default="", metadata={"desc": "系统"})
    
    class Meta:
        collection_name = "SystemInit"


class SystemInitDao(AsyncMongoMethod):
    """
    使用方法： 异步AsyncMongoMethod， 需指定_model_class的模型方法
    """

    _model_class = SystemInit


if __name__ == '__main__':
    print(SystemDept().dump({"status": SystemStatusEnum.p0.value, "status1": "12313"}))
