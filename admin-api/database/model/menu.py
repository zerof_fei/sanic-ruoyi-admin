# -*- coding: utf-8 -*-
"""
@Author：mengying
@file： menu.py
@date：2023/12/19 10:12
@email: 652044581@qq.com
@desc: 系统菜单数据库模型
"""

from marshmallow import Schema, fields
from database.mongoClient import AsyncMongoMethod


class SystemMenu(Schema):
    """
    @description: 系统菜单表
    """
    menuId = fields.String(dump_default="", metadata={"desc": "菜单ID"})
    menuName = fields.String(dump_default="", metadata={"desc": "菜单名称"})
    parentId = fields.String(dump_default="", metadata={"desc": "父菜单ID"})
    orderNum = fields.Integer(dump_default=0, metadata={"desc": "显示顺序"})
    path = fields.String(dump_default="", metadata={"desc": "路由地址"})
    component = fields.String(dump_default="", metadata={"desc": "组件路径"})
    query = fields.String(dump_default="", metadata={"desc": "路由参数"})
    isFrame = fields.String(dump_default="1", metadata={"desc": "是否为外链（0是 1否）"})
    isCache = fields.String(dump_default="0", metadata={"desc": "是否缓存（0缓存 1不缓存）"})
    menuType = fields.String(dump_default="", metadata={"desc": "菜单类型（M目录 C菜单 F按钮）"})
    visible = fields.String(dump_default="0", metadata={"desc": "菜单状态（0显示 1隐藏）"})
    status = fields.String(dump_default="0", metadata={"desc": "菜单状态（0正常 1停用）"})
    perms = fields.String(dump_default="", metadata={"desc": "权限标识"})
    icon = fields.String(dump_default="", metadata={"desc": "菜单图"})
    remark = fields.String(dump_default="", metadata={"desc": "备注"})
    createBy = fields.String(dump_default="", metadata={"desc": "创建者"})

    class Meta:
        collection_name = "SystemMenu"


class SystemMenuDao(AsyncMongoMethod):
    """
    使用方法： 异步AsyncMongoMethod， 需指定_model_class的模型方法
    """

    _model_class = SystemMenu


if __name__ == '__main__':
    print(SystemMenu().dump({"parentId": 0}))
