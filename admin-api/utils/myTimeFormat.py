# -*- coding: utf-8 -*- 
"""
@Author: 孟颖
@email: 652044581@qq.com
@date: 2023/4/23 10:03
@desc: 时间类，生成各类的时间和时间格式转换
"""
import time
from datetime import datetime, timedelta


class MyTime:
    dateTimeType = '%Y-%m-%d %H:%M:%S'
    dateType = '%Y-%m-%d'
    timeType = '%H:%M:%S'
    fileTimeType = "%Y%m%d%H%M%S%f"

    @staticmethod
    def TimeFormat(type: str = dateTimeType) -> str:
        return datetime.now().strftime(type)

    @staticmethod
    def timestampFormat(long: bool = False) -> int:
        return int(time.time() * 1000) if long else int(time.time())

    @staticmethod
    def offsetFormat(start_time: datetime = datetime.now(), days: int = 0, hour: int = 0, minute: int = 0,
                     second: int = 0, type: str = dateTimeType) -> str:
        offsetDateTime = start_time + timedelta(days=days, hours=hour, minutes=minute, seconds=second)
        return offsetDateTime.strftime(type)


if __name__ == '__main__':
    print(MyTime.TimeFormat(MyTime.fileTimeType))
