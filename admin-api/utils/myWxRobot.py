# -*- coding: UTF-8 -*-
"""
========================================================================================================================
@Author:mengying
@File:weixin_SnedMessage.py
@Time:2021-05-25
@Email:a652044581@qq.com
@Features: 企业微信机器人的消息通知
========================================================================================================================
"""

import requests
import base64
import hashlib
from config import config


class WxRobotHook:

    @staticmethod
    def process(response, *args, **kwargs) -> requests.Response:
        print("返回中间件")
        return response


class WxRobot:
    """
    @param: webhook 微信机器人的hook码
    @param: msg 微信机器人发送的消息, 用户send_text方法和send_markdown
    @param: file_path 微信机器人发送的文件（图片或者文件）
    """

    def __init__(self, webhook=None, msg=None, file_path=None):
        self.msg = msg
        self.bot = webhook or getattr(config, "WEBHOOK")
        self.file_path = file_path

    def send_text(self) -> str:
        url = f"https://qyapi.weixin.qq.com/cgi-bin/webhook/send?key={self.bot}"
        headers = {"Content-Type": "text/plain"}
        data = {
            "msgtype": "text",
            "text": {
                "content": self.msg,
            }
        }
        return requests.post(url, headers=headers, hooks=dict(response=WxRobotHook.process), json=data).text

    def send_markdown(self) -> str:
        url = f"https://qyapi.weixin.qq.com/cgi-bin/webhook/send?key={self.bot}"
        data = {
            "msgtype": "markdown",
            "markdown": {"content": self.msg}
        }
        return requests.post(url, hooks=dict(response=WxRobotHook.process), json=data).text

    def send_file(self) -> str:
        file_url = f"https://qyapi.weixin.qq.com/cgi-bin/webhook/upload_media?key={self.bot}&type=file"
        file = {'file': open(self.file_path, 'rb')}
        result = requests.post(file_url, files=file)
        file_id = eval(result.text)['media_id']
        url = f"https://qyapi.weixin.qq.com/cgi-bin/webhook/send?key={self.bot}"
        data = {
            "msgtype": "file",
            "file": {"media_id": file_id, }
        }
        return requests.post(url, hooks=dict(response=WxRobotHook.process), json=data).text

    def send_image(self) -> str:
        with open(self.file_path, "rb") as f:
            base64_data = base64.b64encode(f.read())
        file = open(self.file_path, "rb")
        md5 = hashlib.md5(file.read()).hexdigest()
        url = f"https://qyapi.weixin.qq.com/cgi-bin/webhook/send?key={self.bot}"
        headers = {"Content-Type": "text/plain"}
        data = {
            "msgtype": "image",
            "image": {
                "base64": base64_data.decode('utf-8'),
                "md5": md5
            }
        }
        return requests.post(url, headers=headers, hooks=dict(response=WxRobotHook.process), json=data).text


if __name__ == '__main__':
    bot = "webhook_key"
    msg = "msg"
    robot = WxRobot(webhook=bot, msg=msg)
    robot.send_text()
