# -*- coding: utf-8 -*-
"""
========================================================================================================================
@Author: 孟颖
@email: 652044581@qq.com
@date: 2023/4/20 10:19
@desc: 枚举模块自定义dict转换
========================================================================================================================
"""
from enum import Enum, unique


class EnumDict(Enum):

    @classmethod
    def transform(cls):
        res = dict()
        for key, value in cls.__members__.items():
            res[key] = value.value
        return res

    @classmethod
    def reverse_transform(cls):
        res = dict()
        for key, value in cls.__members__.items():
            res[value.value] = key
        return res

    @classmethod
    def format_front(cls):
        return [{"label": key, "value": value.value} for key, value in cls.__members__.items()]

    @classmethod
    def get_value(cls, key):
        enum_map = cls.transform()
        return enum_map.get(key)


# 示例， 继承EnumDict
@unique
class MaintenanceEnum(EnumDict):
    p1 = "发起授权申请"
    p2 = "绑定授权码"
    p3 = "作废授权码。作废原因："
    p4 = "发起迁移申请"


if __name__ == '__main__':
    print(MaintenanceEnum.transform())

