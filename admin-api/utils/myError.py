# -*- coding: utf-8 -*- 
"""
========================================================================================================================
@project : my-sanic
@file: myError
@Author: mengying
@email: 652044581@qq.com
@date: 2023/3/3 10:32
@desc: 定义系统所需要报错类型, 可以重写日志记录和异常通知处理
========================================================================================================================
"""
from types import FunctionType
from utils.myLog import SimpleLog


class MyError(BaseException):
    """
    @param: message 错误消息
    @param: error_hook 错误的钩子函数类型， 默认接收函数
    @desc: 所有错误的基类， 默认调用钩子，异常处理和日志
    """

    def __init__(self, message, error_hook=None):
        self.message = message
        self.hook = error_hook
        if isinstance(self.hook, FunctionType):
            self.hook.__call__(self)
        self.error_handler()
        self.log_record()

    def error_handler(self):
        pass

    def log_record(self):
        SimpleLog.error("%s, 错误消息: %s" % (self.__class__.__name__, str(self.message)))


class RunTimeError(MyError):
    """自定义错误子类， 调用默认的处理方法就是不处理"""

    def __init__(self, message, error_hook=None):
        super(RunTimeError, self).__init__(message, error_hook=error_hook)


if __name__ == '__main__':
    def error_hook1(error):
        print("error_hook1" + error.message)


    raise RunTimeError('运行时错误', error_hook=error_hook1)
