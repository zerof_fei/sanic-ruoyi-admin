# -*- coding: utf-8 -*-
"""
@Author：mengying
@file： urls.py
@date：2023/12/19 17:53
@email: 652044581@qq.com
@desc: 登陆认证相关
"""
from sanic import Blueprint
from api.mobile import views


# 注册蓝图
MobileBlueprint: Blueprint = Blueprint('mobile', url_prefix='/mobile')


urlpatterns = [
    # 登陆相关
    MobileBlueprint.add_route(views.RegisterView.as_view(), "/register", name="注册"),
    MobileBlueprint.add_route(views.LoginView.as_view(), "/login", name="登录"),
    MobileBlueprint.add_route(views.LogoutView.as_view(), "/logout", name="退出登录"),
    MobileBlueprint.add_route(views.GetInfoView.as_view(), "/getInfo", name="获取登陆信息"),
]