# -*- coding: utf-8 -*-
"""
@Author：mengying
@file： urls.py
@date：2023/12/19 17:53
@email: 652044581@qq.com
@desc: 登陆认证相关
"""
from sanic import Blueprint
from api.login import views


# 注册蓝图
LoginBlueprint: Blueprint = Blueprint('login', url_prefix='/')


urlpatterns = [
    # 登陆相关
    LoginBlueprint.add_route(views.CaptchaImageView.as_view(), "/captchaImage", name="获取验证码"),
    LoginBlueprint.add_route(views.LoginView.as_view(), "/login", name="登录"),
    LoginBlueprint.add_route(views.LogoutView.as_view(), "/logout", name="退出登录"),
    LoginBlueprint.add_route(views.GetInfoView.as_view(), "/getInfo", name="获取登陆信息"),
    LoginBlueprint.add_route(views.GetRoutersView.as_view(), "/getRouters", name="获取用户权限路由"),
]