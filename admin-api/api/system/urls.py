# -*- coding: utf-8 -*- 
"""
========================================================================================================================
@project : my-sanic
@file: urls
@Author: mengying
@email: 652044581@qq.com
@date: 2023/3/21 14:47
@desc: 
========================================================================================================================
"""
from sanic import Blueprint
from api.system import views

# 注册蓝图
SystemBlueprint: Blueprint = Blueprint('system', url_prefix='/system')

urlpatterns = [

    # 部门相关
    SystemBlueprint.add_route(views.DeptListView.as_view(), "/dept/list", name="部门列表"),
    SystemBlueprint.add_route(views.DeptCreateView.as_view(), "/dept", name="部门新增"),
    SystemBlueprint.add_route(views.DeptDetailView.as_view(), "/dept/<deptId:[0-9]+>", name="部门详情/删除"),
    SystemBlueprint.add_route(views.DeptExcludeView.as_view(), "/dept/list/exclude/<deptId:[0-9]+>", name="排除查询"),

    # 角色相关
    SystemBlueprint.add_route(views.RoleListView.as_view(), "/role/list", name="角色列表"),
    SystemBlueprint.add_route(views.RoleCreateView.as_view(), "/role", name="角色新增"),
    SystemBlueprint.add_route(views.RoleStatusView.as_view(), "/role/changeStatus", name="角色状态修改"),
    SystemBlueprint.add_route(views.RoleDetailView.as_view(), "/role/<roleId:[0-9 ,]+>", name="角色详情/删除"),

    # 角色认证
    SystemBlueprint.add_route(views.AuthUserAllocatedListView.as_view(), "/role/authUser/allocatedList",
                              name="分配用户列表"),
    SystemBlueprint.add_route(views.AuthUserCancelView.as_view(), "/role/authUser/cancel", name="角色取消人员授权"),
    SystemBlueprint.add_route(views.AuthUserCancelAllView.as_view(), "/role/authUser/cancelAll", name="批量取消授权"),
    SystemBlueprint.add_route(views.AuthUserUnallocatedListView.as_view(), "/role/authUser/unallocatedList",
                              name="未授权用户列表"),
    SystemBlueprint.add_route(views.AuthUserSelectAllView.as_view(), "/role/authUser/selectAll", name="批量绑定角色人员"),

    SystemBlueprint.add_route(views.AuthRoleView.as_view(), "/user/authRole/<userId:[0-9]+>", name="用户角色绑定"),

    # 用户相关
    SystemBlueprint.add_route(views.UserDeptTreeView.as_view(), "/user/deptTree", name="用户部门"),
    SystemBlueprint.add_route(views.UserListView.as_view(), "/user/list", name="用户列表"),
    SystemBlueprint.add_route(views.UserStatusView.as_view(), "/user/changeStatus", name="用户状态修改"),
    SystemBlueprint.add_route(views.UserCreateView.as_view(), "/user", name="用户新增"),
    SystemBlueprint.add_route(views.UserDetailView.as_view(), "/user/<userId:[0-9 ,]+>", name="用户详情/删除"),
    SystemBlueprint.add_route(views.UserResetPwdView.as_view(), "/user/resetPwd", name="重置密码"),

    # 个人中心
    SystemBlueprint.add_route(views.UserUpdatePwdView.as_view(), "/user/profile/updatePwd", name="修改密码"),
    SystemBlueprint.add_route(views.UserProfileView.as_view(), "/user/profile", name="个人中心"),
    SystemBlueprint.add_route(views.UserProfileAvatarView.as_view(), "/user/profile/avatar", name="修改头像"),

    # 目录相关
    SystemBlueprint.add_route(views.MenuListView.as_view(), "/menu/list", name="目录列表"),
    SystemBlueprint.add_route(views.MenuDetailView.as_view(), "/menu/<menuId:[0-9]+>", name="目录详情/删除"),
    SystemBlueprint.add_route(views.MenuCreateView.as_view(), "/menu", name="目录新增"),
    SystemBlueprint.add_route(views.MenuTreeSelectView.as_view(), "/menu/treeselect", name="树选择器"),
    SystemBlueprint.add_route(views.MenuRoleTreeSelectView.as_view(), "/menu/roleMenuTreeselect/<roleId:[0-9]+>",
                              name="已选择的树选择器"),

]
