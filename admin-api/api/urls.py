# -*- coding: utf-8 -*- 
"""
========================================================================================================================
@project : my-sanic
@file: __init__
@Author: mengying
@email: 652044581@qq.com
@date: 2023/3/7 13:45
@desc: 
========================================================================================================================
"""
from sanic import Blueprint
from api.system import urls as systemUri
from api.login import urls as loginUri
from api.mobile import urls as mobileUri

# 项目的api的根路由
urlpatterns = [
    systemUri.SystemBlueprint,  # 系统内置蓝图
    loginUri.LoginBlueprint,  # 系统内置蓝图
    mobileUri.MobileBlueprint, # 移动端蓝图
]


routers = Blueprint.group(*urlpatterns, url_prefix='/api')
